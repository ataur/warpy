'use strict';

var path = require('path');
var env = {
    development: require('./dev.json'),
    production: require('./prod.json'),
    test: require('./test.json')
};

var envVariables = env[process.env.NODE_ENV] || env.development;
var httpConf = require('./identity/http.json');


module.exports = {
    appName: envVariables.appName,
    appVersion: envVariables.appVersion,
    scriptVersion: envVariables.scriptVersion,
    apiVersion: envVariables.apiVersion,
    auth: envVariables.auth,
    ip: httpConf.ip,
    domain: httpConf.domain,
    port: httpConf.port,
    httpsPort: httpConf.httpsPort,
    httpUrl: httpConf.httpUrl,
    httpsUrl: httpConf.httpsUrl,
    cdn: httpConf.cdnUrl,
    cdnUrlHttps: httpConf.cdnUrlHttps,
    publicUrl: envVariables.publicUrl,
    mysql: envVariables.mysql,
    secret: envVariables.secret,
    serverDir: path.normalize(__dirname + '/..'),
    publicDir: path.normalize(__dirname + '/../public'),
    googlePlacesKey:envVariables.google.placesApiKey
};