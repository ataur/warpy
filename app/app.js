'use strict';

var http = require('http');
var express = require('express');
var db = require('./models');

var defaultCtrl = require('./controllers/default');
var restaurantCtrl = require('./controllers/restaurants');

console.log(restaurantCtrl);

var configs = require('./configs');
var logger = require('./logger');
var log = logger.defaultLogger;
var promise = require('bluebird');

function Server() {

    var self = this;
    this.app = express();

    // configure middleware for express
    this.setupExpressMiddleware = function () {

        self.app.use(logger.requestLogger);
        self.app.disable('x-powered-by');

        self.app.use(express.static(configs.publicDir, { maxAge: '1d' }));

        self.app.use(function (req, res, next) {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,PATCH,DELETE,OPTIONS');
            res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, X-Requested-With, x-access-token');
            next();
        });

    };

    // database authentication and models migration
    this.loadModels = function () {
        db.sequelize.authenticate().then(function () {
            if (configs.mysql.sequelize.sync) {
                promise.each(db.models, function (modelName) {
                    return db[modelName].sync();
                }).then(function (names) {
                    log.info(names + ' created');
                }).error(function (err) {
                    log.error(err.message);
                });
            }
        
        }).error(function (err) {
            log.error(err);
        });
    };

    // routes and resource
    this.defineRoutes = function () {

        self.app.options('*', function (req, res) {
            res.status(200).end();
        });
        restaurantCtrl(this.app);
        defaultCtrl.routes(this.app);
    };


    // catch termination signal
    this.terminator = function (signal) {
        if (typeof signal === 'string') {
            log.warn('Received signal %s - terminating %s', signal, configs.appName);
            process.exit(1);
        }
        log.warn('Node app has stopped');
    };

    // termination handlers
    this.terminationHandlers = function () {
        process.on('exit', function () {
            self.terminator();
        });

        ['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
            'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
        ].forEach(function (element) {
                process.on(element, function () {
                    self.terminator(element);
                });
            });
    };

    // start the app
    this.start = function () {
        this.terminationHandlers();
        http.createServer(this.app).listen(configs.port);
        log.info('%s has started on http://%s:%s', configs.appName, configs.domain, configs.port);
    };
}




var server = new Server();
server.loadModels();
server.setupExpressMiddleware();
server.defineRoutes();
server.start();
