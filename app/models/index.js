'use strict';

var fs = require('fs'),
    path = require('path'),
    Sequelize = require('sequelize'),
    config = require('../configs'),
    db = {},
    dialectOptions = null,
    replicationConfigs = {};

// Sequelize itself enable pooling when it gets non-empty pool object,
// properties into define are application to all models,
// replication config will be added when it is required
replicationConfigs = false;

var sequelize = new Sequelize(
    config.mysql.database,
    config.mysql.server.master.username,
    config.mysql.server.master.password, {
        host: config.mysql.server.master.host,
        port: config.mysql.server.master.port,
        replication : replicationConfigs,
        pool: {
            maxConnections: config.mysql.minPoolConnections,
            minConnections: config.mysql.maxPoolConnections
        },
        define: {
            timestamps: true,
            charset: config.mysql.charset,
            collate: config.mysql.collate
        },
        logging: config.mysql.sequelize.logging,
        dialect: config.mysql.sequelize.dialect,
        dialectOptions: dialectOptions
    }
);

// just storing models string names
db.models = [];

// read models from the folder except index
fs.readdirSync(__dirname).filter(function (file) {
    return (file.indexOf('.js') > 0) && (file !== 'index.js');
}).forEach(function (file) {
    var model = sequelize['import'](path.join(__dirname, file));
    db[model.name] = model;
    db.models.push(model.name);
});

// As we have foreign key constrain,
// therefore models should follow correct order when syncing
db.models.sort(function(a, b){
    return db[a].serial - db[b].serial;
});

// Association of relational models
Object.keys(db).forEach(function (modelName) {
    if ('associate' in db[modelName]) {
        db[modelName].associate(db);
    }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = exports = db;