'use strict';

module.exports = function(sequelize, DataTypes){

    let Review = sequelize.define('Review', {
        id : {
            type: DataTypes.INTEGER,
            unique: true,
            primaryKey: true
        },
        rating : {
            type: DataTypes.INTEGER,
            allowNull:false,
            defaultValue:1
        },
        review : {
            type: DataTypes.STRING(1024),
            allowNull:true
        }
    }, {
        tableName : 'warp_reviews',

        classMethods : {
            serial: 1
        },
        createdAt : 'createdAt',
        updatedAt: 'updatedAt'

    });

    return Review;
};
