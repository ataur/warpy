'use strict';

var Warpy = angular.module('Warpy', [
    'ui.router',
    'ngAnimate',
    'ngSanitize',
    'ui.bootstrap',
    'google.places'
]);

Warpy.config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider','$urlMatcherFactoryProvider',
    '$provide',
    function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $urlMatcherFactoryProvider,
        provide) {

        // For any unmatched url, redirect to /404
        /*$urlRouterProvider.otherwise("/404"); */
        $urlRouterProvider.when('', '/');
        $urlRouterProvider.when('/','/restaurants/search');

    
        //Traling slash needs to be added
        //$urlMatcherFactoryProvider.strictMode(false);

        // Now set up the states
        $stateProvider
        .state('main', {
            abstract:true,
            url: '',
            templateUrl: 'common/views/content.html'
        })
        .state('main.restaurants', {
            abstract: true,
            url: '/restaurants',
            template: '<div ui-view></div>'
        })
        .state('main.restaurants.search', {
            url: '/search',
            controller: 'searchCtrl',
            controllerAs:'vm',
            templateUrl: 'modules/restaurants/search/search.html'
        })
        .state('main.restaurants.place', {
            url: '/restaurant/:id',
            controller: 'restaurantCtrl',
            controllerAs:'vm',
            templateUrl: 'modules/restaurants/restaurant/restaurant.html',
            resolve:{
            	'restaurant':['$http','$stateParams','restaurantModel',function($http,$stateParams,restaurantModel){
            		var placeId = $stateParams.id;
            		return restaurantModel.getRestaurant(placeId);
            	}]
            }
        })
        
        // No # on router, pretty. And helps seo
        $locationProvider.html5Mode(true).hashPrefix('!');
        //$urlRouterProvider.deferIntercept();
    }
]);

angular
    .module('Warpy')
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
        $rootScope.globalConfigs = globalConfigs;
    });