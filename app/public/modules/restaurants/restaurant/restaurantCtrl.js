(function(){
'use strict';
  var Warpy = angular.module('Warpy');
  Warpy.controller('restaurantCtrl', ['$scope', '$location','restaurantModel','restaurant',
    function($scope, $location, restaurantModel, restaurant) {
    	var vm = this;
        vm.restaurant = restaurant;
        
        vm.saveReview = function(){
            console.log(vm.restaurant.place_id,vm.rating,vm.review);
        	restaurantModel.saveReview(vm.restaurant.place_id,vm.rating,vm.review)
        	.then(function(data){
        		vm.rating = null;
        		vm.review = '';
        		alert('Saved');
        	})
        }
        
	}]);
})();