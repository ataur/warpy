(function(){

'use strict';

    var Warpy = angular.module('Warpy');
    Warpy.factory('restaurantModel', ['$http', '$q',
        function ($http, $q) {

            function getRestaurants(lat,lng) {

                var params = {};
                params.lat = lat;
                params.lng = lng;

                return $http.get('/api/restaurants',{params:params})
                .then(function(data){
                    return data.data;
                })
            }

            function getRestaurant(id) {
                return $http.get('/api/restaurants/'+id)
                .then(function(data){
                    if(data.data.success){
                        return data.data.restaurant;
                    }
                    return {};
                })
            }

            function saveReview(id,rating,review) {
                return $http.put('/api/restaurants/'+id+'/review',{rating:rating,review:review})
            }

            return {
                getRestaurants: getRestaurants,
                getRestaurant:getRestaurant,
                saveReview:saveReview
            };
        }
    ]);
})();
