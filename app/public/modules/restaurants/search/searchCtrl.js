(function(){
'use strict';
  var Warpy = angular.module('Warpy');
  Warpy.controller('searchCtrl', ['$scope', '$location','restaurantModel',
    function($scope, $location, restaurantModel) {
    	var vm = this;
    	vm.googlePlace = {};
    	vm.googlePlace.options = {};
    	vm.googlePlace.selected = null;
    	vm.restaurants = [];
    	vm.loadingRestaurants = false;

    	vm.loadRestaurants = function(lat,lng) {
    		vm.loadingRestaurants = true;
    		restaurantModel.getRestaurants(lat,lng)
    		.then(function(data){
    			vm.loadingRestaurants = false;
    			if(data.success){
    				vm.restaurants = data.restaurants;
    			}
    		})
    	}

    	vm.searchedForPlace = function(selectedPlace) {
    		if(selectedPlace && selectedPlace.geometry && selectedPlace.geometry.location){
    			vm.restaurants = [];
    			var lat = selectedPlace.geometry.location.lat();
    			var lng = selectedPlace.geometry.location.lng();
    			vm.loadRestaurants(lat,lng);
    		}
    	}

    	$scope.$on('g-places-autocomplete:select', function (event, param) {
	        vm.searchedForPlace(param)
	    });


		function getLocation() {
		  if (navigator.geolocation) {
		    navigator.geolocation.getCurrentPosition(getLatLng);
		  }
		}

		function getLatLng(position) {
		  var lat = position.coords.latitude;
		  var lng = position.coords.longitude;
		  vm.loadRestaurants(lat,lng);
		}

		getLocation();

	    
	}]);
})();