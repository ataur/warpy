'use strict';

const expect    = require('chai').expect;
const request = require('request');

let base = 'http://localhost:3005/api/restaurants';

describe('Getting list of restaurants based on location', function() {
	this.timeout(15000);

  	it('returns status 200',function(done) {
      request.get(base,{lat:'23.810332',lng:'90.4125181'},function(error, response, body) {
        expect(response.statusCode).to.equal(200);
        done();
      });
    });

    it('request is successfull',function(done) {
      request.get({url:base,qs:{lat:'23.810332',lng:'90.4125181',json:true}},function(error, response, body) {
        expect(JSON.parse(body)).to.have.property('success').and.equal(true);
        done();
      });
    });

});