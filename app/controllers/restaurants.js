'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();
const log = require('./../logger').defaultLogger;
const configs = require('../configs');
const db = require('../models');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));



let googleMapsClient = require('@google/maps').createClient({
  key: configs.googlePlacesKey,
  Promise: require('bluebird')
});

/**
 * @apiGroup 01. Restaurants
 * @api {GET} /api/restaurants 01. Get Restaurants
 * @apiVersion 1.0.1
 * @apiDescription Get list of restaurants from google places api
 * query string specified in parameter block.
 * @apiParam {lat} [lat] latitue of a location
 * @apiParam {lng} [lng] longitude of a location
 * @apiExample {curl} Curl :
 *    curl 'http://localhost/api/restaurants/localhost?lat=22&lng=90'
 */
router.get('/', function (req, res) {

    let lat = req.query.lat;
    let lng = req.query.lng;


    googleMapsClient.placesNearby({
        location:{lat:lat,lng:lng},
        rankby:'distance',
        type:'restaurant'
    })
    .asPromise()
    .then(function(places){
        var restaurants = [];
        if(places.json && places.json.results) {
            restaurants = places.json.results;
        }
        res.json({success:true,restaurants:restaurants});
    })
    .catch(function(error){
        log.error(error);
        res.json({success:false});
    })
    
});

/**
 * @apiGroup 01. Restaurants
 * @api {GET} /api/restaurant/:id 02. Get Restaurant
 * @apiVersion 1.0.1
 * @apiDescription Get list of restaurants from google places api
 * query string specified in parameter block.
 * @apiParam {placeId} [placeId] google place id
 * @apiExample {curl} Curl :
 *    curl 'http://localhost/api/restaurants/<place-id>'
 */
router.get('/:id', function (req, res) {

    var placeId = req.params.id;

    googleMapsClient.place({
        placeid:placeId
    })
    .asPromise()
    .then(function(place){
        var restaurant = [];
        if(place.json && place.json.result) {
            restaurant = place.json.result;
        }
        res.json({success:true,restaurant:restaurant});
    })
    .catch(function(error){
        log.error(error);
        res.json({success:false});
    })
    
});

/**
 * @apiGroup 01. Restaurants
 * @api {PUT} /api/restaurants/<id>/review 03. Add Review
 * @apiVersion 1.0.1
 * @apiDescription Add Review
 * @apiParam {String} review Review
 * @apiParam {Number} rating Rating
 * @apiExample {curl} Curl :
 *    curl -X PUT -H "Content-Type: application/json" \
 *        -d '{"review": "Good restaurant", "rating": "5"}' \
 *       "http://localhost/api/restaurants/<id>/review"
 */
router.put('/:id/review',function(req, res){
    var rating = req.body.rating;
    var review = req.body.review;

    db.Review.create({review:review,rating:rating})
    .then(function(){
        res.json({success:true});
    })
    .catch(function(error){
        res.json({success:false});
    })
});


module.exports = function (app) {
    app.use('/api/restaurants', router);
};