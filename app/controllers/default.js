'use strict';

const fs = require('fs');
const promise = require('bluebird');
const configs = require('../configs');
const log = require('./../logger').defaultLogger;

exports.routes = function(app) {

      // deliver initial html page, which has required frontend scripts
      app.get('*', function (req, res) {
        console.log(process.env.NODE_ENV)
       var primaryLayout = (process.env.NODE_ENV && process.env.NODE_ENV === 'production') ? 'production.html' : 'development.html';
       fs.readFile(configs.publicDir + '/' + primaryLayout, 'utf8', function(err, html){
         if(!err){
           _injectConfig(html).then(function(view){
            res.setHeader('content-type', 'text/html');
            res.status(200).send(view);
           });
         } else {
           log.error(err.message);
           res.status(500).end('An unexptected error occured');
         }
       });
     });

    function _injectConfig(view){
      var varObj = {};

      varObj.appName = configs.appName;
      varObj.appVersion = configs.appVersion;
      varObj.apiVersion = configs.apiVersion;
      varObj.domain = configs.domain;
      varObj.domainUrl = configs.httpsUrl;
      varObj.cdnUrl = configs.cdn;
      varObj.scriptVersion = configs.scriptVersion;
      varObj.apiUrl = configs.httpUrl + '/' + configs.apiVersion ;
      varObj.googlePlacesKey = configs.googlePlacesKey ;

      return new promise(function(resolve){
        var pattern = new RegExp('{{.*' + Object.keys(varObj).join('*.}}|{{.*') + '*.}}', 'gi');
        var html = view.replace(pattern, function(match){
          return varObj[match.replace(new RegExp('}}|{{', 'g'), '').trim()];
        });
        resolve(html);
      });
    }

  

};