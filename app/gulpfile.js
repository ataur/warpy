'use strict';

const gulp       = require('gulp');
const nodemon    = require('gulp-nodemon');
const less = require('gulp-less');
const rename = require('gulp-rename');
const livereload = require('gulp-livereload');
const lesshint = require('gulp-lesshint');
const jshint = require('gulp-jshint');
const mocha = require('gulp-mocha');

let jsPaths = [
    './conntrollers/*.js',
    './logger/*.js',
    './models/*.js',
    './services/*.js',
    './public/app.js',
    './public/modules/**/*.js',
    './public/common/**/*.js'
];

gulp.task('develop', function () {
  nodemon({script: './app.js', ext: 'js hjs json',  env: { 'NODE_ENV': 'development' } });
});


gulp.task('less', function(done) {
  gulp.src('./public/assets/styles/less/styles.less')
    .pipe(less())
    .pipe(gulp.dest('./public/assets/styles/css'))
    .pipe(rename('styles.css'))
    .pipe(gulp.dest('./public/assets/styles/css'))
    .pipe(livereload())
    .on('end', function(){
      done();
    })
    .on('error', function (err) {
      console.log(err.message);
    });
});

gulp.task('less-lint', function(done) {
  gulp.src('./public/assets/styles/less/styles.less')
  .pipe(lesshint({}))
  .pipe(lesshint.reporter())
  .on('error', function (err) {
      console.log('-----------------css lint ------------------');
      console.log(err.message);
      console.log('-----------------css lint ------------------');
  });
});

gulp.task('js-lint', function(done) {
  gulp.src(jsPaths)
  .pipe(jshint())
  .pipe(jshint.reporter('default'))
  .on('error', function (err) {
      console.log('-----------------js lint ------------------');
      console.log(err.message);
      console.log('-----------------js lint ------------------');
  });
});

gulp.task('watch',function () {
    livereload.listen();
    gulp.watch('./public/assets/styles/less/**/*.less', ['less','less-lint']);
    gulp.watch([jsPaths], ['js-lint']);
});

gulp.task('test', function () {
   gulp.src('./tests/restaurant.js', {read: false})
    .pipe(mocha({reporter: 'nyan'})) 
})


gulp.task('default', ['develop','less','js-lint','less-lint','watch'],function(){
  gulp.src('./tests/restaurant.js', {read: false})
    .pipe(mocha({reporter: 'nyan'}))
    .on('error',function(err){
      console.log(err);
    })
});