# WARP TEST
``git@bitbucket.org:ataur/warpy.git``

App is built using Express, Nginx, AngularJS, Google Places Api and MySql, Mocha, Less, Bootstrap with Gulp build system and Docker.
    
### Installation
##### Development
  - Make sure you have docker and docker-compose installed 
  - Go to Project directory
  - Run ``docker-compose up --build``
  - App will run in http://localhost
 
*run ``docker-compose up`` again if you have mysql connection issue.

This will automatically Setup:

  * Nginx with reverese proxy
  * Mysql with schema
  * Auto server update on file changes using nodemon
  * Auto linting, available via ``gulp less-lint`` ``gulp js-lint`` on file change, .jshintrc and .lesshintrc is used so you can integrate with your editor/ide.
  * Mocha tests on ``gulp test``
  * Additionally ``gulp watch`` is available for ^^ commands
  * One should use gulp from the container rather the host: docker exec -it {{containerId}} /bin/bash

##### Production
   - run ``docker-compose up --file docker-compose-prod.yml ``
   - We can use Amazon ECS to deploy it easily: http://blog.valiantys.com/en/expert-tips/deploying-docker-containers-aws

### APIs
Api Doc is available in: app/doc folder. Created with http://apidocjs.com/



*google places api has some limits when looking for multiple photos at a time, I have put unsplash.com's as a placholder for restaurant images.